package pe.marcolopez.apps.skeleton.api.dao.seguridad;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Sesion;

@Repository
public interface SesionRepository extends CrudRepository<Sesion, Integer> {
}
