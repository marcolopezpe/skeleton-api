package pe.marcolopez.apps.skeleton.api.dao.seguridad;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Optional<Usuario> findByUsuarioIgnoreCase(String usuario);

    Optional<Usuario> findByEmailIgnoreCase(String email);
}
