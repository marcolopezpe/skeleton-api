# skeleton-api

Repository Skeleton API o base para iniciar nuevos proyectos desde cero en API REST con Spring Boot.

Elaborado con:
- JDK 1.8 (Amazon Corretto)
- Spring Boot 2.3.1 (Spring Data JPA, Spring Security, OAuth2, JWT, Hibernate Core/Envers, Spring Framework)
- PostgreSQL 11
- Lombok 1.18 (Se debe configurar en el IDE)
- Apache Tomcat 9.0.34

Crear el usuario: `skeleton`, ya que se usa como `owner` de la base de datos y sus objetos.

Para iniciar la Base de Datos:
- Crear con el nombre `db_skeleton`.
- Restaurarla con el archivo `db_skeleton.backup`, haciendo Click Derecho y `Restore` con `pgAdmin4` y seleccionando 
`skeleton` como usuario owner. Aqui seleccionar el archivo con extensi&oacute;n .backup.

Para ejecutar el proyecto, se debe configurar la variable de entorno `(Environment Variable)` en el Servidor 
`Tomcat 9.0.x` que hemos instalado en nuestro IDE.

La variable de entorno se encuentra en `ConfigAPIProperties`, con el valor `propertySkeleton`. Este deber&aacute; tener 
como valor la ruta de la carpeta donde se encuentran los properties: `${skeleton-api}/libraries/resources/properties`, 
donde: `${skeleton-api}` es la ruta de la carpeta del proyecto y `libraries` el m&oacute;dulo donde está la carpeta 
`properties`. 

Las configuraciones para el API est&aacute;n en `config-api.properties` y para la Base de Datos en 
`config-db.properties`, dentro del modulo `libraries`.

Para consultas, me puedes encontrar en <a href="http://marcolopez.pe">marcolopez.pe</a>.

---

Info:

```
Credenciales de API (las contraseñas estan en BCrypt):
- Tabla: public.oauth_client_details
- client_id: skeleton_client
- contraseña: skeleton

Credenciales de Usuario:
- Tabla: seguridad.usuario
- Usuario: admin
- Contraseña: 123456
```

&copy; 2020
