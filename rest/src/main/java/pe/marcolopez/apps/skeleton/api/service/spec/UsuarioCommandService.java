package pe.marcolopez.apps.skeleton.api.service.spec;

import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioCreateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioUpdateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;

public interface UsuarioCommandService {

    Integer createUsuario(UsuarioCreateDTO usuarioCreateDTO);

    UsuarioQueryDTO updateUsuario(Integer id, UsuarioUpdateDTO usuarioUpdateDTO);

    void deleteUsuario(Integer id);
}
