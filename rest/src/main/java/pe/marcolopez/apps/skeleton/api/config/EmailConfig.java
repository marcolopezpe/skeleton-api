package pe.marcolopez.apps.skeleton.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import pe.marcolopez.apps.skeleton.api.properties.ConfigAPIProperties;

import java.util.Properties;

@Configuration
public class EmailConfig {

    private final ConfigAPIProperties properties;

    @Autowired
    public EmailConfig(ConfigAPIProperties properties) {
        this.properties = properties;
    }

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(properties.getServer().getMail().getHost());
        mailSender.setPort(properties.getServer().getMail().getPort());
        mailSender.setUsername(properties.getServer().getMail().getEmail());
        mailSender.setPassword(properties.getServer().getMail().getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", properties.getServer().getMail().getProtocol());
        props.put("mail.smtp.auth", properties.getServer().getMail().getAuth());
        props.put("mail.smtp.starttls.enable", properties.getServer().getMail().getStarttls());
        props.put("mail.smtp.ssl.trust", "*");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        mailSender.setJavaMailProperties(props);

        return mailSender;
    }
}
