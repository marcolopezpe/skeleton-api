package pe.marcolopez.apps.skeleton.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.marcolopez.apps.skeleton.api.dao.seguridad.UsuarioRepository;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Rol;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;
import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioCreateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioUpdateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;
import pe.marcolopez.apps.skeleton.api.service.spec.UsuarioCommandService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioCommandServiceImpl implements UsuarioCommandService {

    private final UsuarioRepository usuarioRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UsuarioCommandServiceImpl(UsuarioRepository usuarioRepository, BCryptPasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public Integer createUsuario(UsuarioCreateDTO usuarioCreateDTO) {
        Usuario newUsuario = new Usuario();

        newUsuario.setNombre(usuarioCreateDTO.getNombre());
        newUsuario.setUsuario(usuarioCreateDTO.getUsuario());
        newUsuario.setEmail(usuarioCreateDTO.getEmail());
        newUsuario.setActivado(usuarioCreateDTO.isActivado());
        newUsuario.setContrasena(passwordEncoder.encode(usuarioCreateDTO.getContrasena()));

        try {
            return usuarioRepository.save(newUsuario).getUsuarioId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional
    public UsuarioQueryDTO updateUsuario(Integer id, UsuarioUpdateDTO usuarioUpdateDTO) {
        if (usuarioRepository.findById(id).isPresent()) {
            Usuario existingUsuario = usuarioRepository.findById(id).get();

            existingUsuario.setNombre(usuarioUpdateDTO.getNombre());
            existingUsuario.setUsuario(usuarioUpdateDTO.getUsuario());
            existingUsuario.setEmail(usuarioUpdateDTO.getEmail());
            existingUsuario.setActivado(usuarioUpdateDTO.isActivado());

            List<Rol> roles = new ArrayList<>();
            usuarioUpdateDTO.getRoles().forEach(rolUpdateDTO -> {
                Rol rol = new Rol();
                rol.setRolId(rolUpdateDTO.getId());
                rol.setNombre(rolUpdateDTO.getNombre());
                roles.add(rol);
            });
            existingUsuario.setRoles(roles);

            Usuario updatedUsuario = usuarioRepository.save(existingUsuario);
            return UsuarioQueryDTO.getInstance(updatedUsuario);
        }
        return null;
    }

    @Override
    @Transactional
    public void deleteUsuario(Integer id) {
        if (usuarioRepository.findById(id).isPresent()) {
            usuarioRepository.deleteById(id);
        }
    }
}
