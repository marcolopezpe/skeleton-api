package pe.marcolopez.apps.skeleton.api.rest.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(description = "Modelo para leer la inbformaci\u00F3n de un Usuario")
public class UsuarioQueryDTO implements Serializable {

    private static final long serialVersionUID = 5459364606528508231L;

    @ApiModelProperty(notes = "ID del Usuario")
    private Integer id;

    @ApiModelProperty(notes = "Nombre del Usuario")
    private String nombre;

    @ApiModelProperty(notes = "Usuario ID del Usuario")
    private String usuario;

    @ApiModelProperty(notes = "Email del Usuario")
    private String email;

    @ApiModelProperty(notes = "Campo Activado del Usuario")
    private boolean activado;

    @ApiModelProperty(notes = "Roles del Usuario")
    private List<RolQueryDTO> roles;

    public static UsuarioQueryDTO getInstance(Usuario usuario) {
        if (usuario == null) return null;

        UsuarioQueryDTO usuarioQueryDTO = new UsuarioQueryDTO();
        usuarioQueryDTO.setId(usuario.getUsuarioId());
        usuarioQueryDTO.setNombre(usuario.getNombre());
        usuarioQueryDTO.setUsuario(usuario.getUsuario());
        usuarioQueryDTO.setEmail(usuario.getEmail());
        usuarioQueryDTO.setActivado(usuario.isActivado());
        usuarioQueryDTO.setRoles(RolQueryDTO.getInstance(usuario.getRoles()));

        return usuarioQueryDTO;
    }
}
