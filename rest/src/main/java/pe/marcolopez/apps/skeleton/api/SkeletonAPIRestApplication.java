package pe.marcolopez.apps.skeleton.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SkeletonAPIRestApplication extends SpringBootServletInitializer {

    public static void main(String... args) {
        SpringApplication.run(SkeletonAPIRestApplication.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SkeletonAPIRestApplication.class);
    }
}
