package pe.marcolopez.apps.skeleton.api.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;
import pe.marcolopez.apps.skeleton.api.properties.ConfigAPIProperties;
import pe.marcolopez.apps.skeleton.api.properties.ConfigDBProperties;
import pe.marcolopez.apps.skeleton.api.security.DefaultUserDetailsService;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final ConfigAPIProperties apiProperties;
    private final ConfigDBProperties dbProperties;
    private final DefaultUserDetailsService defaultUserDetailsService;

    @Autowired
    public WebSecurityConfig(ConfigAPIProperties apiProperties, ConfigDBProperties dbProperties, DefaultUserDetailsService defaultUserDetailsService) {
        this.apiProperties = apiProperties;
        this.dbProperties = dbProperties;
        this.defaultUserDetailsService = defaultUserDetailsService;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .httpBasic()
                .realmName(apiProperties.getServer().getApi().getJwt().getSecurityRealm())
            .and()
                .csrf().disable()
                .cors().disable();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(defaultUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter() {

            @Override
            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
                Map<String, Object> additionalInformation = new HashMap<>();
                Usuario usuario = (Usuario) authentication.getPrincipal();
                additionalInformation.put("nombre", usuario.getNombre());
                additionalInformation.put("usuario", usuario.getUsuario());
                additionalInformation.put("email", usuario.getEmail());
                additionalInformation.put("activado", usuario.isActivado());
                additionalInformation.put("bloqueado", usuario.isBloqueado());
                additionalInformation.put("contrasenaExpirada", usuario.isContrasenaExpirada());
                ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
                return super.enhance(accessToken, authentication);
            }
        };
        converter.setSigningKey(apiProperties.getServer().getApi().getJwt().getSigningKey());
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        DataSource tokenDataSource = DataSourceBuilder.create()
                .driverClassName(dbProperties.getDataSource().getDriver())
                .username(dbProperties.getDataSource().getUsername())
                .password(dbProperties.getDataSource().getPassword())
                .url(dbProperties.getDataSource().getUrl())
                .build();
        return new JdbcTokenStore(tokenDataSource);
    }

    @Bean
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        /*
        Indica si se admite el refresh_token. El valor predeterminado es false.
         */
        defaultTokenServices.setSupportRefreshToken(true);
        /*
        Indica si se debe re-usar el refresh_token. El valor predeterminado es true.
        (si es falso, cada actualización de solicitud eliminará el antiguo refresh_token y creará un nuevo refresh_token)
         */
        defaultTokenServices.setReuseRefreshToken(false);
        return defaultTokenServices;
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
    }
}
