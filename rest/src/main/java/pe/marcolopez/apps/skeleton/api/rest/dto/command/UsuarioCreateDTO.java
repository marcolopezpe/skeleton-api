package pe.marcolopez.apps.skeleton.api.rest.dto.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ApiModel(description = "Modelo para crear un nuevo Usuario")
public class UsuarioCreateDTO implements Serializable {

    private static final long serialVersionUID = -1038023729182039354L;

    @NotBlank(message = "El Nombre del Usuario es obligatorio")
    @Size(min = 3, message = "El Nombre del Usuario debe tener al menos 3 caracteres")
    @ApiModelProperty(notes = "Nombre del Usuario", example = "John Doe")
    private String nombre;

    @NotBlank(message = "El Usuario ID del Usuario es obligatorio")
    @Size(min = 5, message = "El Usuario ID del Usuario debe tener al menos 5 caracteres")
    @ApiModelProperty(notes = "Usuario ID del Usuario, que se ulitiza para autenticarse", example = "john.doe")
    private String usuario;

    @Email(message = "El Formato debe ser de un Email correcto")
    @NotBlank(message = "El Email del Usuario es obligatorio")
    @Size(min = 10, message = "El Email del Usuario debe tener al menos 10 caracteres")
    @ApiModelProperty(notes = "Email del Usuario, donde se env\u00EDa lo correos", example = "john.doe@example.com")
    private String email;

    @NotBlank(message = "La Contrase\u00F1a del Usuario es obligatoria")
    @Size(min = 8, message = "La Contrase\u00F1a del Usuario debe tener al menos 8 caracteres")
    @ApiModelProperty(notes = "Contrase\u00F1a del Usuario")
    private String contrasena;

    @NotBlank(message = "La confirmacion de la Contrase\u00F1a es obligatoria")
    @Size(min = 8, message = "La confirmaci\u00F3n Contrase\u00F1a del Usuario debe tener al menos 8 caracteres")
    @ApiModelProperty(notes = "Confirmaci\u00F3n de la Contrase\u00F1a del Usuario")
    private String confirmarContrasena;

    @ApiModelProperty(notes = "Campo Activado del Usuario")
    private boolean activado;
}
