package pe.marcolopez.apps.skeleton.api.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.marcolopez.apps.skeleton.api.dao.seguridad.UsuarioRepository;
import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;
import pe.marcolopez.apps.skeleton.api.service.spec.UsuarioQueryService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UsuarioQueryServiceImpl implements UsuarioQueryService {

    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioQueryServiceImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UsuarioQueryDTO getUsuario(Integer id) {
        if (usuarioRepository.findById(id).isPresent()) {
            return UsuarioQueryDTO.getInstance(usuarioRepository.findById(id).get());
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public UsuarioQueryDTO getUsuarioByUsuario(String usuario) {
        if (usuarioRepository.findByUsuarioIgnoreCase(usuario).isPresent()) {
            return UsuarioQueryDTO.getInstance(usuarioRepository.findByUsuarioIgnoreCase(usuario).get());
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public UsuarioQueryDTO getUsuarioByEmail(String email) {
        if (usuarioRepository.findByEmailIgnoreCase(email).isPresent()) {
            return UsuarioQueryDTO.getInstance(usuarioRepository.findByEmailIgnoreCase(email).get());
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UsuarioQueryDTO> getUsuarios() {
        List<UsuarioQueryDTO> usuariosQueryDTO = new ArrayList<>();
        usuarioRepository.findAll().forEach(usuario -> usuariosQueryDTO.add(UsuarioQueryDTO.getInstance(usuario)));
        return usuariosQueryDTO;
    }
}
