package pe.marcolopez.apps.skeleton.api.exception;

public class DeleteEntityException extends Exception {

    private static final long serialVersionUID = 1L;

    public DeleteEntityException() {
    }

    public DeleteEntityException(String message) {
        super(message);
    }

    public DeleteEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
