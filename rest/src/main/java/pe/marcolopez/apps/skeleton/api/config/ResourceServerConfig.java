package pe.marcolopez.apps.skeleton.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import pe.marcolopez.apps.skeleton.api.properties.ConfigAPIProperties;
import pe.marcolopez.apps.skeleton.api.security.DefaultAuthenticationEntryPoint;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private final ConfigAPIProperties properties;

    @Autowired
    public ResourceServerConfig(ConfigAPIProperties properties) {
        this.properties = properties;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(properties.getServer().getApi().getJwt().getResourceIDs());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                    .accessDeniedHandler(new OAuth2AccessDeniedHandler())
                    .authenticationEntryPoint(new DefaultAuthenticationEntryPoint())
                .and()
                    .requestMatchers()
                .and()
                .authorizeRequests()
                    .antMatchers("/v2/api-docs/**").permitAll()
                    .antMatchers("/recuperar**").permitAll()
                    .antMatchers("/token**").permitAll()
                    .antMatchers("/api/**").fullyAuthenticated()
                .and()
                    .cors().disable();
    }
}
