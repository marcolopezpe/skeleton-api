package pe.marcolopez.apps.skeleton.api.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DefaultUserDetails extends Usuario implements UserDetails {

    private static final long serialVersionUID = -6413851796530310670L;

    public DefaultUserDetails(Usuario usuario) {
        super(usuario);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        super.getRoles().forEach(rol -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(rol.getNombre()));
            rol.getPermisos().forEach(permiso -> grantedAuthorities.add(new SimpleGrantedAuthority(permiso.getNombre())));
        });

        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return super.getContrasena();
    }

    @Override
    public String getUsername() {
        return super.getUsuario();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !super.isExpirado();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !super.isBloqueado();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !super.isContrasenaExpirada();
    }

    @Override
    public boolean isEnabled() {
        return super.isActivado();
    }
}
