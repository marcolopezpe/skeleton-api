package pe.marcolopez.apps.skeleton.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.marcolopez.apps.skeleton.api.dao.seguridad.UsuarioRepository;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Usuario;

import java.util.Optional;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

    private final UsuarioRepository usuarioRepository;

    @Autowired
    public DefaultUserDetailsService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Usuario> optionalUsuario = usuarioRepository.findByUsuarioIgnoreCase(username);
        if (optionalUsuario.isPresent()) {
            return new DefaultUserDetails(optionalUsuario.get());
        } else {
            throw new UsernameNotFoundException("Username or password wrong");
        }
    }
}
