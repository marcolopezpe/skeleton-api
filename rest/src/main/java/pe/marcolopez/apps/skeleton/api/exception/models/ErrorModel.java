package pe.marcolopez.apps.skeleton.api.exception.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ErrorModel {

    private HttpStatus httpStatus;

    private LocalDateTime timestamp = LocalDateTime.now();

    private String message;

    private String details;

    public ErrorModel(HttpStatus httpStatus, String message, String details) {
        this.httpStatus = httpStatus;
        this.message = message;
        this.details = details;
    }
}
