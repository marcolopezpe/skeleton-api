package pe.marcolopez.apps.skeleton.api.rest.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
public class LoginController {

    @Resource(name = "tokenServices")
    private ConsumerTokenServices tokenServices;

    @PostMapping("oauth/revoke")
    public ResponseEntity<?> revokeToken(@RequestBody String tokenValue) {
        boolean tokenIsRevoked = tokenServices.revokeToken(tokenValue);
        if (tokenIsRevoked) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
