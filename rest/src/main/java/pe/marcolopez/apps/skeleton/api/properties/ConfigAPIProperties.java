package pe.marcolopez.apps.skeleton.api.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("config-api")
@PropertySource("file:${propertySkeleton}/config-api.properties")
public class ConfigAPIProperties {

    private ServerProperties server;

    @Data
    public static class ServerProperties {

        private String ip;
        private Integer port;
        private String contextPath;
        private String urlContextPath;
        private APIProperties api;
        private MailProperties mail;
        private JobProperties job;
    }

    @Data
    public static class APIProperties {

        private String username;
        private String password;
        private String title;
        private String description;
        private String version;
        private String terms;
        private String contactName;
        private String contactUrl;
        private String contactEmail;
        private String license;
        private String licenseUrl;
        private JWTProperties jwt;
    }

    @Data
    public static class JWTProperties {

        private String signingKey;
        private String encodingStrengh;
        private String securityRealm;
        private String resourceIDs;
    }

    @Data
    public static class MailProperties {

        private String email;
        private String password;
        private String alias;
        private String host;
        private String protocol;
        private Integer port;
        private Boolean auth;
        private Boolean starttls;
        private String emailInfo;
        private Integer tokenTime;
        private EmailTemplatesProperties templates;
    }

    @Data
    public static class EmailTemplatesProperties {

        private String recuperarContrasenia;
    }

    @Data
    public static class JobProperties {

        private String frequency;
    }
}
