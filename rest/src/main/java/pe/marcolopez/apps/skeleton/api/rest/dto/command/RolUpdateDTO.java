package pe.marcolopez.apps.skeleton.api.rest.dto.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ApiModel(description = "Model para actualizar un Rol existente")
public class RolUpdateDTO implements Serializable {

    private static final long serialVersionUID = -8070670869032721185L;

    @NotBlank(message = "El ID del Rol es obligatorio")
    @Size(min = 1, message = "El ID del Rol debe tener al menos 1 caracter")
    @ApiModelProperty(notes = "ID del Rol", example = "1")
    private Integer id;

    @NotBlank(message = "El Nombre del Rol es obligatorio")
    @Size(min = 3, message = "El Nombre del Rol debe tener al menos 3 caracteres")
    @ApiModelProperty(notes = "Nombre del Rol", example = "USER")
    private String nombre;
}
