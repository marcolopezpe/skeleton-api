package pe.marcolopez.apps.skeleton.api.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pe.marcolopez.apps.skeleton.api.exception.models.ErrorModel;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOG.error("### handleMethodArgumentNotValid <- {0}", ex);

        ErrorModel error = new ErrorModel(HttpStatus.BAD_REQUEST, "Error de Validacion", ex.getBindingResult().toString());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOG.error("### handleExceptionInternal <- {0}", ex);

        ErrorModel error = new ErrorModel(HttpStatus.CONFLICT, "Error Interno", ex.toString());
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    private ResponseEntity<ErrorModel> handleEntityNotFound(EntityNotFoundException ex) {
        LOG.error("### handleEntityNotFound <- {0}", ex);

        ErrorModel error = new ErrorModel(HttpStatus.NOT_FOUND, "Data no encontrada, consulte con soporte.", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PersistenceException.class)
    private ResponseEntity<ErrorModel> handlePersistence(PersistenceException ex) {
        LOG.error("### handlePersistence <- {0}", ex);

        ErrorModel error = new ErrorModel(HttpStatus.INTERNAL_SERVER_ERROR, "Error en Base de Datos, consulte con soporte.", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DeleteEntityException.class)
    private ResponseEntity<ErrorModel> handleDeleteEntity(DeleteEntityException ex) {
        LOG.error("### handleDeleteEntity <- {0}", ex);

        ErrorModel error = new ErrorModel(HttpStatus.CONFLICT, "Error al eliminar el registro.", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }
}