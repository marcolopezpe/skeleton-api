package pe.marcolopez.apps.skeleton.api.rest.controllers.query;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;
import pe.marcolopez.apps.skeleton.api.service.spec.UsuarioQueryService;

import java.util.List;

@RestController
@RequestMapping("api/usuarios")
@Api(tags = "Usuario Queries", value = "UsuarioQueryController")
public class UsuarioQueryController {

    private final UsuarioQueryService usuarioQueryService;

    @Autowired
    public UsuarioQueryController(UsuarioQueryService usuarioQueryService) {
        this.usuarioQueryService = usuarioQueryService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Listar Usuarios")
    public ResponseEntity<List<UsuarioQueryDTO>> getUsuarios() {
        return new ResponseEntity<>(usuarioQueryService.getUsuarios(), HttpStatus.OK);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Usuario por ID")
    public ResponseEntity<UsuarioQueryDTO> getUsuario(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(usuarioQueryService.getUsuario(id), HttpStatus.OK);
    }

    @GetMapping(value = "usuario/{usuario}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Usuario por ID")
    public ResponseEntity<UsuarioQueryDTO> getUsuarioByUsuario(@PathVariable("usuario") String usuario) {
        return new ResponseEntity<>(usuarioQueryService.getUsuarioByUsuario(usuario), HttpStatus.OK);
    }

    @GetMapping(value = "email/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Usuario por ID")
    public ResponseEntity<UsuarioQueryDTO> getUsuarioByEmail(@PathVariable("email") String email) {
        return new ResponseEntity<>(usuarioQueryService.getUsuarioByEmail(email), HttpStatus.OK);
    }
}
