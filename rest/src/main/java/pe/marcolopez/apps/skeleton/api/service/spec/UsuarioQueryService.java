package pe.marcolopez.apps.skeleton.api.service.spec;

import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;

import java.util.List;

public interface UsuarioQueryService {

    UsuarioQueryDTO getUsuario(Integer id);

    UsuarioQueryDTO getUsuarioByUsuario(String usuario);

    UsuarioQueryDTO getUsuarioByEmail(String email);

    List<UsuarioQueryDTO> getUsuarios();

}
