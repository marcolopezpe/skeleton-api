package pe.marcolopez.apps.skeleton.api.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("config-db")
@PropertySource("file:${propertySkeleton}/config-db.properties")
public class ConfigDBProperties {

    private DataSourceProperties dataSource;

    @Data
    public static class DataSourceProperties {

        private String server;
        private String database;
        private String driver;
        private String url;
        private String username;
        private String password;
        private Boolean autoCommit;
        private Integer maximumPoolSize;
        private String poolName;
        private Integer connectionTimeout;
        private Integer minimumIdle;
    }
}
