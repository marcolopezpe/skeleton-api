package pe.marcolopez.apps.skeleton.api.rest.dto.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(description = "Modelo para actualizar un Usuario existente")
public class UsuarioUpdateDTO implements Serializable {

    private static final long serialVersionUID = -5397318336411335551L;

    @NotBlank(message = "El Nombre del Usuario es obligatorio")
    @Size(min = 3, message = "El Nombre del Usuario debe tener al menos 3 caracteres")
    @ApiModelProperty(notes = "Nombre del Usuario", example = "John Doe")
    private String nombre;

    @NotBlank(message = "El Usuario ID del Usuario es obligatorio")
    @Size(min = 5, message = "El Usuario ID del Usuario debe tener al menos 5 caracteres")
    @ApiModelProperty(notes = "Usuario ID del Usuario, que se ulitiza para autenticarse", example = "john.doe")
    private String usuario;

    @Email(message = "El Formato debe ser de un Email correcto")
    @NotBlank(message = "El Email del Usuario es obligatorio")
    @Size(min = 10, message = "El Email del Usuario debe tener al menos 10 caracteres")
    @ApiModelProperty(notes = "Email del Usuario, donde se env\u00EDa lo correos", example = "john.doe@example.com")
    private String email;

    @ApiModelProperty(notes = "Campo Activado del Usuario")
    private boolean activado;

    @ApiModelProperty(notes = "Roles del Usuario")
    private List<RolUpdateDTO> roles;
}
