package pe.marcolopez.apps.skeleton.api.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import pe.marcolopez.apps.skeleton.api.properties.ConfigDBProperties;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    private final ConfigDBProperties properties;

    @Autowired
    public DataSourceConfig(ConfigDBProperties properties) {
        this.properties = properties;
    }

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(properties.getDataSource().getDriver());
        hikariConfig.setJdbcUrl(properties.getDataSource().getUrl());
        hikariConfig.setUsername(properties.getDataSource().getUsername());
        hikariConfig.setPassword(properties.getDataSource().getPassword());
        hikariConfig.setPoolName(properties.getDataSource().getPoolName());
        hikariConfig.setMaximumPoolSize(properties.getDataSource().getMaximumPoolSize());
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
}
