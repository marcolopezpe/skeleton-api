package pe.marcolopez.apps.skeleton.api.rest.controllers.command;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioCreateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.command.UsuarioUpdateDTO;
import pe.marcolopez.apps.skeleton.api.rest.dto.query.UsuarioQueryDTO;
import pe.marcolopez.apps.skeleton.api.service.spec.UsuarioCommandService;

import javax.validation.Valid;

@RestController
@RequestMapping("api/usuarios")
@Api(tags = "Usuario Commands", value = "UsuarioCommandController")
public class UsuarioCommandController {

    private final UsuarioCommandService usuarioCommandService;

    @Autowired
    public UsuarioCommandController(UsuarioCommandService usuarioCommandService) {
        this.usuarioCommandService = usuarioCommandService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create una nuevo Usuario")
    public ResponseEntity<?> createUsuario(@Valid @RequestBody UsuarioCreateDTO usuarioCreateDTO) {
        if (usuarioCreateDTO.getContrasena().equals(usuarioCreateDTO.getConfirmarContrasena())) {
            return new ResponseEntity<>(usuarioCommandService.createUsuario(usuarioCreateDTO), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Las Contrase\u00F1as no coinciden.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Actualiza un Usuario existente")
    public ResponseEntity<UsuarioQueryDTO> updateUsuario(@PathVariable("id") Integer id,
                                                         @Valid @RequestBody UsuarioUpdateDTO usuarioUpdateDTO) {
        return new ResponseEntity<>(usuarioCommandService.updateUsuario(id, usuarioUpdateDTO), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Eliminar un Usuario existente")
    public ResponseEntity<?> deleteUsuario(@PathVariable("id") Integer id) {
        usuarioCommandService.deleteUsuario(id);
        return ResponseEntity.ok().build();
    }
}
