package pe.marcolopez.apps.skeleton.api.rest.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import pe.marcolopez.apps.skeleton.api.model.seguridad.Rol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(description = "Model para leer la informaci\u00F3n de un Rol")
public class RolQueryDTO implements Serializable {

    private static final long serialVersionUID = -7546799314527017483L;

    @ApiModelProperty(notes = "ID del Rol")
    private Integer id;

    @ApiModelProperty(notes = "Nombre del Rol")
    private String nombre;

    public static RolQueryDTO getInstance(Rol rol) {
        if (rol == null) return null;

        RolQueryDTO rolQueryDTO = new RolQueryDTO();
        rolQueryDTO.setId(rol.getRolId());
        rolQueryDTO.setNombre(rol.getNombre());

        return rolQueryDTO;
    }

    public static List<RolQueryDTO> getInstance(List<Rol> roles) {
        if (roles == null) return null;

        List<RolQueryDTO> rolesQueryDTO = new ArrayList<>();
        roles.forEach(rol -> rolesQueryDTO.add(RolQueryDTO.getInstance(rol)));

        return rolesQueryDTO;
    }
}
