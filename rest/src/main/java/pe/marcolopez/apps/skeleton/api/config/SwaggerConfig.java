package pe.marcolopez.apps.skeleton.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.marcolopez.apps.skeleton.api.properties.ConfigAPIProperties;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private final ConfigAPIProperties properties;
    private final static String BASE_PACKAGE = "pe.marcolopez.apps.skeleton.api.rest.controllers";

    @Autowired
    public SwaggerConfig(ConfigAPIProperties properties) {
        this.properties = properties;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.getApiInformation())
                .useDefaultResponseMessages(Boolean.FALSE)
                .globalResponseMessage(RequestMethod.GET, getCustomizedResponseMessages());
    }

    private ApiInfo getApiInformation() {
        return new ApiInfo(
                properties.getServer().getApi().getTitle(),
                properties.getServer().getApi().getDescription(),
                properties.getServer().getApi().getVersion(),
                properties.getServer().getApi().getTerms(),
                new Contact(properties.getServer().getApi().getContactName(),
                        properties.getServer().getApi().getContactUrl(),
                        properties.getServer().getApi().getContactEmail()),
                properties.getServer().getApi().getLicense(),
                properties.getServer().getApi().getLicenseUrl(),
                Collections.emptyList()
        );
    }

    private List<ResponseMessage> getCustomizedResponseMessages() {
        List<ResponseMessage> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseMessageBuilder().code(500).message("Server has crashed!").responseModel(new ModelRef("Error")).build());
        responseMessages.add(new ResponseMessageBuilder().code(403).message("You shall not pass!!").build());
        return responseMessages;
    }
}
