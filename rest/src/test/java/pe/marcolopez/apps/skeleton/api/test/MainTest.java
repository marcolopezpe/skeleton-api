package pe.marcolopez.apps.skeleton.api.test;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class MainTest {

    public static void main(String[] args) {
        int i = 0;
        while (i < 10) {
            String password = "P@ssw0rd";
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(password);

            System.out.println(hashedPassword);
            i++;
        }
    }
}
