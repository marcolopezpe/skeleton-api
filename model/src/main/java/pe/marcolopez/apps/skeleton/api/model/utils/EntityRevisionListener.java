package pe.marcolopez.apps.skeleton.api.model.utils;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pe.marcolopez.apps.skeleton.api.model.auditoria.UsuarioRevisionEntity;

public class EntityRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object o) {
        UsuarioRevisionEntity usuarioRevisionEntity = (UsuarioRevisionEntity) o;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        usuarioRevisionEntity.setUsername(authentication.getName());
    }
}
