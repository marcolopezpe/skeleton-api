package pe.marcolopez.apps.skeleton.api.model.auditoria;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<T> implements Serializable {

    private static final long serialVersionUID = -3845401467552402612L;

    @CreatedBy
    @Column(name = "usuario_creacion")
    protected T usuarioCreacion;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_creacion")
    protected Date fechaCreacion;

    @LastModifiedBy
    @Column(name = "usuario_modificacion")
    protected T usuarioModificacion;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_modificacion")
    protected Date fechaModificacion;
}
