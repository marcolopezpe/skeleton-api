package pe.marcolopez.apps.skeleton.api.model.seguridad;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import pe.marcolopez.apps.skeleton.api.model.auditoria.Auditable;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Audited
@Data
@Entity
@Table(name = "usuario", schema = "seguridad")
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Usuario extends Auditable<String> {

    private static final long serialVersionUID = -807205271047412386L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuario_id")
    @EqualsAndHashCode.Include
    private Integer usuarioId;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "usuario", unique = true)
    private String usuario;

    @Column(name = "contrasena")
    private String contrasena;

    @Column(name = "email")
    private String email;

    @Column(name = "activado")
    private boolean activado;

    @Column(name = "expirado")
    private boolean expirado;

    @Column(name = "bloqueado")
    private boolean bloqueado;

    @Column(name = "contrasena_expirada")
    private boolean contrasenaExpirada;

    @NotAudited
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "usuario_rol", schema = "seguridad",
            joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "rol_id")
    )
    private List<Rol> roles;

    public Usuario(Usuario parUsuario) {
        this.nombre = parUsuario.nombre;
        this.usuario = parUsuario.usuario;
        this.contrasena = parUsuario.contrasena;
        this.email = parUsuario.email;
        this.activado = parUsuario.activado;
        this.expirado = parUsuario.expirado;
        this.bloqueado = parUsuario.bloqueado;
        this.roles = parUsuario.roles;
    }
}
