package pe.marcolopez.apps.skeleton.api.model.seguridad;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "permiso", schema = "seguridad")
@Data
public class Permiso implements Serializable {

    private static final long serialVersionUID = 2358802944701181781L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "permiso_id")
    private Integer permisoId;

    @Column(name = "nombre")
    private String nombre;
}
