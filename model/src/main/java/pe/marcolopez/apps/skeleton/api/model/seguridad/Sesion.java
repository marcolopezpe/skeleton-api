package pe.marcolopez.apps.skeleton.api.model.seguridad;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sesion", schema = "seguridad")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sesion implements Serializable {

    private static final long serialVersionUID = 1378148460868019489L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sesion_id")
    @EqualsAndHashCode.Include
    private Integer sesionId;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "direccion_ip")
    private String direccionIP;

    @Column(name = "pais")
    private String pais;

    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @ManyToOne(targetEntity = Usuario.class)
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
}
