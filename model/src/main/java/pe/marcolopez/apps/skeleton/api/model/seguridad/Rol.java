package pe.marcolopez.apps.skeleton.api.model.seguridad;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "rol", schema = "seguridad")
@Data
@EqualsAndHashCode
public class Rol implements Serializable {

    private static final long serialVersionUID = -7412579182991135374L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rol_id")
    @EqualsAndHashCode.Include
    private Integer rolId;

    @Column(name = "nombre")
    private String nombre;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rol_permiso", schema = "seguridad",
            joinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "rol_id"),
            inverseJoinColumns = @JoinColumn(name = "permiso_id", referencedColumnName = "permiso_id")
    )
    private List<Permiso> permisos;
}
