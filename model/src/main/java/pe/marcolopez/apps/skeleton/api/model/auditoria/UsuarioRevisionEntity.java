package pe.marcolopez.apps.skeleton.api.model.auditoria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;
import pe.marcolopez.apps.skeleton.api.model.utils.EntityRevisionListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "usuario_revision", schema = "auditoria")
@RevisionEntity(EntityRevisionListener.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioRevisionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @RevisionNumber
    @Column(name = "revision_id")
    private Integer id;

    @RevisionTimestamp
    @Column(name = "revision_timestamp")
    private Long timestamp;

    @Column(name = "revision_usuario", length = 30)
    private String username;

    @Transient
    public Date getRevisionDate() {
        return new Date(this.timestamp);
    }
}
